﻿using UnityEngine;
using System.Collections;

public class HandlerDamage : MonoBehaviour {
	public int health = 1;

	public float invulnPeriod = 0;
	float invulnTimer = 0;
	int correctLayer;

	SpriteRenderer damnIt;

	void Start (){
		correctLayer = gameObject.layer;
		damnIt = GetComponent<SpriteRenderer> ();
		if (damnIt == null) {
			damnIt = transform.GetComponentInChildren<SpriteRenderer>();
			if(damnIt == null){
				Debug.LogError("Object'"+gameObject.name+"' has no sprite renderer");
			}
		}
	}

	void OnTriggerEnter2D(){
			health--;
			invulnTimer = 1.05f;
		if(invulnPeriod > 0){
			invulnTimer = invulnPeriod;
			gameObject.layer = 10;
		}
	}
	void Update (){


		if(invulnTimer <= 0){
			invulnTimer -= Time.deltaTime;

			if(invulnTimer <= 0){
				gameObject.layer = correctLayer;
				if (damnIt != null){
					damnIt.enabled = true;
				}
				}else{
					if(damnIt != null)
					{
						damnIt.enabled = !damnIt.enabled;
					}
				}
		}

		if (health <= 0) {
			Die();
		}
		}
	void Die(){
		Destroy (gameObject);
	}

}